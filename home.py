

import home
import state
import type_vehicle

import streamlit as st

#HOME PAGE
def app():
    st.title('Home')

    header1, header2 = st.columns((2, 1))

    with header1:
        st.title('study of my google data 🗺️')
        st.write("data visualization project")

    with header2:
        st.markdown(f"<h3 style='text-align:right;'>create by jean-benoit Charbonnaud</h3>", unsafe_allow_html=True)
        st.markdown(f"<h5 style='text-align:right;'>accompany by Severin Poncin</h5>", unsafe_allow_html=True)
        st.markdown(f"<h5 style='text-align:right;'>and Mano Joseph Mathew</h5>", unsafe_allow_html=True)

    st.markdown("The goal of this project is to show all the geographic data that google collects on you. It was a very fun project to do and it allowed me to learn a lot about data visualization.")
    st.markdown("Now i let you find out everything google knows about me.")

    expander = st.expander("FAQ")
    expander.write("Here you could put in some really, really long explanations...")
