# -*- coding: utf-8 -*-

import streamlit as st
# To make things easier later, we're also importing numpy and pandas for
# working with sample data.
import numpy as np
import pandas as pd
import pydeck as pdk
import os
from datetime import datetime as dt
import matplotlib.pyplot as plt
import altair as alt
import seaborn as sns
import webbrowser

import home
import how
import state
import type_vehicle
import collection
import InTheTime

###################################################################################################

st.set_page_config(
    page_title="mydata",
    layout='wide'
    )

st_folder = 'sp_app_solution'
os.makedirs(st_folder, exist_ok=True)



# MENU

PAGES = {
    "home": home,
    "how" : how,
    "collection of my data": collection,
    "geographic data ": state,
    "my types of travel": type_vehicle,
    "time": InTheTime
}
st.sidebar.title('Menu')
selection = st.sidebar.radio("Select your page", list(PAGES.keys()))
page = PAGES[selection]
page.app()
















    
    



  