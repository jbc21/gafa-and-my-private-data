import numpy as np
import pandas as pd
import pydeck as pdk
import os
from datetime import datetime as dt
import matplotlib.pyplot as plt
import altair as alt
import seaborn as sns
import webbrowser
import streamlit as st
import csv

import home
import state
import type_vehicle

def app():

    
    # open dataframe
    df_gps = pd.read_csv('data.csv')
    st.title('geographic data 🌎')

    st.markdown(
        f"<h1 style='text-align:center;color:red ;'>more than {len(df_gps)} gps coordinates collected 📍</h1></br></br>",
        unsafe_allow_html=True)

    
    # plot dataframe
    def my_widget():
        st.dataframe(df_gps.head())
    my_expander = st.expander("My dataframe", expanded=False)
    with my_expander:
        clicked = my_widget()
    


    #clean dataframe
    df_gps.drop_duplicates(subset="datetime", keep=False, inplace=True)
    df_gps.drop_duplicates(subset="lat", keep=False, inplace=True)
    df_gps.drop_duplicates(subset="lon", keep=False, inplace=True)

    st.title('studies of the most frequented places')
    bourgogne, paris = st.columns(2)

    with bourgogne:

        #mask for bourgogne
        data_bourgogne1 = df_gps['lat'] < 47.44
        data_bourgogne2 = df_gps['lat'] > 46.57
        data_bourgogne4 = df_gps['lon'] < 5.37
        data_bourgogne3 = df_gps['lon'] > 4.14

        st.title('data bourgogne 🏠')


        #map bourgogne
        st.pydeck_chart(pdk.Deck(
            map_style='mapbox://styles/mapbox/light-v9',
            initial_view_state=pdk.ViewState(
                latitude=46.85,
                longitude=4.77,
                zoom=11,
                pitch=50,
            ),
            layers=[
                pdk.Layer(
                    'HexagonLayer',
                    data=df_gps[data_bourgogne1][data_bourgogne2][data_bourgogne3][data_bourgogne4],
                    get_position='[lon, lat]',
                    radius=50,
                    elevation_scale=40,
                    elevation_range=[0, 100],
                    pickable=True,
                    extruded=True,
                ),

            ],
        ))

    with paris:
        #mask paris
        data_paris1 = df_gps['lat'] < 49.1
        data_paris2 = df_gps['lat'] > 48.57
        data_paris4 = df_gps['lon'] < 2.87
        data_paris3 = df_gps['lon'] > 1.98

        st.title('data paris 🏫')


        #mask paris
        st.pydeck_chart(pdk.Deck(
            map_style='mapbox://styles/mapbox/light-v9',
            initial_view_state=pdk.ViewState(
                latitude=48.86,
                longitude=2.36,
                zoom=9,
                pitch=50,
            ),
            layers=[
                pdk.Layer(
                    'HexagonLayer',
                    data=df_gps[data_paris1][data_paris2][data_paris3][data_paris4],
                    get_position='[lon, lat]',
                    radius=50,
                    elevation_scale=40,
                    elevation_range=[0, 100],
                    pickable=True,
                    extruded=True,

                ),

            ],
        ))

    st.title('data geo')

    data_year_loca, data_month_loca, data_day_loca = st.columns(3)

    #update date to dataframe
    df_gps['datetime'] = pd.to_datetime(df_gps['datetime'], errors='coerce')

    with data_year_loca:
        #creat options
        year_to_filter = st.slider('year', 2016, 2021, 2017)
        filtered_data_year = df_gps[df_gps['datetime'].dt.year == year_to_filter]
        st.subheader('Map of all pickups in %s' % year_to_filter)
        # plot map
        st.map(filtered_data_year)

    with data_month_loca:
        # creat options
        month_to_filter = st.slider('month', 0, 12, 2)
        filtered_data_month = df_gps[df_gps['datetime'].dt.month == month_to_filter]
        st.subheader('Map of all pickups the %s month' % month_to_filter)
        # plot map
        st.map(filtered_data_month)

    with data_day_loca:
        # creat options
        hour_to_filter = st.slider('hour', 0, 23, 17)
        filtered_data_day = df_gps[df_gps['datetime'].dt.hour == hour_to_filter]
        st.subheader('Map of all pickups at %s:00' % hour_to_filter)
        #plot map
        st.map(filtered_data_day)

    # //////////////////////////////////////////////////////////////////

