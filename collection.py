import numpy as np
import pandas as pd
import pydeck as pdk
import os
from datetime import datetime as dt
import matplotlib.pyplot as plt
import altair as alt
import seaborn as sns
import webbrowser
import streamlit as st


import home
import state
import type_vehicle




def app():
    def count_rows(rows):
        return len(rows)

    
    # open dataframe
    df_gps = pd.read_csv('data.csv')
    st.title('Collection 📖')

    st.markdown(
        f"<h1 style='text-align:center;color:red ;'>more than {len(df_gps)} gps coordinates collected 📍</h1></br></br>",
        unsafe_allow_html=True)

    
    # plot dataframe
    def my_widget():
        st.dataframe(df_gps.head())
    my_expander = st.expander("My dataframe", expanded=False)
    with my_expander:
        clicked = my_widget()
    

    st.title('study of data collection over time 📅')
    data_year, data_month, data_day, data_day_name = st.columns(4)

    with data_year:
        by_date = df_gps.groupby('year').apply(count_rows)
        st.markdown("number data by year")
        st.bar_chart(by_date)

    with data_month:
        by_date = df_gps.groupby('month').apply(count_rows)
        st.markdown("number data by month")
        st.bar_chart(by_date)

    with data_day:
        by_date = df_gps.groupby('day').apply(count_rows)
        st.markdown("number data by day")
        st.bar_chart(by_date)

    with data_day_name:
        by_date = df_gps.groupby('dayOfWeek').apply(count_rows)
        st.markdown("number data by day name")
        st.bar_chart(by_date)


    a, b = st.columns((1, 5))

    with a:
        st.markdown(f"<h1></h1></br></br>", unsafe_allow_html=True)
        date = st.radio(
            "choice your year",
            ('2021', '2020', '2019', '2018', '2017', '2016'))

        if date == '2020':
            df2 = df_gps['year'] == 2020
            df2 = df_gps[df2].groupby(['day', 'month']).apply(count_rows).unstack()
        if date == '2021':
            df2 = df_gps['year'] == 2021
            df2 = df_gps[df2].groupby(['day', 'month']).apply(count_rows).unstack()
        if date == '2019':
            df2 = df_gps['year'] == 2019
            df2 = df_gps[df2].groupby(['day', 'month']).apply(count_rows).unstack()
        if date == '2018':
            df2 = df_gps['year'] == 2018
            df2 = df_gps[df2].groupby(['day', 'month']).apply(count_rows).unstack()
        if date == '2017':
            df2 = df_gps['year'] == 2017
            df2 = df_gps[df2].groupby(['day', 'month']).apply(count_rows).unstack()
        if date == '2016':
            df2 = df_gps['year'] == 2016
            df2 = df_gps[df2].groupby(['day', 'month']).apply(count_rows).unstack()

    with b:
        st.title('data day by month')
        fig, ax = plt.subplots(figsize=(50, 22))
        sns.heatmap(df2, ax=ax, cmap='Blues')
        plt.show()
        st.write(fig)
