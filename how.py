import home
import state
import type_vehicle

import streamlit as st
def app():
    st.title('how I carried out my project')
    st.markdown("The first step is to recover the data.")

    how1, how2 = st.columns(2)

    with how1:
        st.image('how1.jpg', width=700)
        st.image('how3.jpg', width=700)
    with how2:
        st.image('how2.jpg', width=700)
        st.image('how4.jpg', width=700)
    how4, how5,how6 = st.columns(3)

    with how5:
        st.image('how5.jpg', width=700)

    st.title('understand the data')
    how1, how2 = st.columns(2)
    with how1:
        st.markdown(f"<p style='color:rgb(155, 200,163) ;'> the place where google spotted me 📍</p>", unsafe_allow_html=True)
        st.markdown(f"<p style='color:rgb(250, 166,26) ;'> how long i am staying on my phone or my computer</p>", unsafe_allow_html=True)
        st.markdown(f"<p style='color:rgb(14, 212, 252) ;'> another point where I could find myself</p>", unsafe_allow_html=True)

    with how2:
        st.markdown(f"<p style='color:rgb(103, 133, 244) ;'> where I started and where I finished</p>", unsafe_allow_html=True)
        st.markdown(f"<p style='color:rgb(255, 200, 134) ;'> the start and end time of my trip</p>", unsafe_allow_html=True)
        st.markdown(f"<p style='color:rgb(157, 81, 255);'> the different vehicles that I use</p>", unsafe_allow_html=True)
        st.markdown(f"<p style='color:rgb(181, 230, 29) ;'> where I went during my trip</h1>", unsafe_allow_html=True)


    how1, how2 = st.columns(2)
    with how1:
        st.image('mydata1.png', width=700)
    with how2:
        st.image('mydata2.png', width=700)