import numpy as np
import pandas as pd
import pydeck as pdk
import os
from datetime import datetime as dt
import matplotlib.pyplot as plt
import altair as alt
import seaborn as sns
import webbrowser
import streamlit as st

import home
import state



def app():
    def get_year(dt):
        return dt.year

    def count_rows(rows):
        return len(rows)

    def get_day(dt):
        return dt.day

    def get_month(dt):
        return dt.month

    def convertToDateString(dt):
        return dt[0].strftime("%d/%m/%Y")

    def columnStack(lat, lon):
        return np.column_stack((lon, lat)).tolist()

    def get_hour(dt):
        return dt.hour

    def get_minute(dt):
        return dt.minute
    
    st.title('data type 🛸')

    # open dataframe
    df3 = pd.read_csv('FULL_activity_points.csv')

    #group data by type
    by_date = df3.groupby('ac_type').apply(count_rows)
    st.markdown("number data by year")
    #plot data by type
    st.bar_chart(by_date)

    
    #plot dataframe
    def my_widget():
        st.dataframe(df3.head())
    my_expander = st.expander("My dataframe", expanded=False)
    with my_expander:
        clicked = my_widget()
    

    #select type of vehicul in the dataframe
    type = df3['ac_type'].unique()

    #creat select option of vehicul in the dataframe
    options = st.multiselect(
        'Select your type of vehicle',
        type, ['FLYING'])

    df2 = df3[df3['ac_type'].isin(options)]

    #clean dataframe
    df2 = df2.drop_duplicates(subset=['time_stamp', 'lat', 'lon'], keep='last')

    #update culomns
    df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
    df2['day'] = df2['time_stamp'].map(get_day)
    df2['month'] = df2['time_stamp'].map(get_month)

    #adapte dataframe for the map
    df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
    df2['date'] = df2['time_stamp'].map(convertToDateString)
    df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

    df = pd.DataFrame({
        "name": df2["date"].values.tolist(),
        "color": [(237, 28 + i, 36) for i in range(len(df2))],
        "path": df2['path'].values.tolist()
    })

    #center map
    view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

    #setting map
    layer = pdk.Layer(
        type="PathLayer",
        data=df,
        pickable=True,
        get_color="color",
        width_scale=1,
        width_min_pixels=1,
        get_path="path",
        get_width=1,
    )
    #plot map
    st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))


    
    a, b, c = st.columns(3)
    with a:

        df2 = df3[df3['ac_type'].isin(["WALKING"])]

        #update map
        df2 = df2.drop_duplicates(subset=['time_stamp','lat','lon'], keep='last')
        df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
        df2['day'] = df2['time_stamp'].map(get_day)
        df2['month'] = df2['time_stamp'].map(get_month)
        df2['year'] = df2['time_stamp'].map(get_year)

        # adapte dataframe for the map
        df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
        df2['date'] = df2['time_stamp'].map(convertToDateString)
        df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

        df = pd.DataFrame({
            "name": df2["date"].values.tolist(),
            "color": [(155, 200,163) for i in range(len(df2))],
            "path": df2['path'].values.tolist()
        })

        st.subheader('WALKING 🚶‍♂️')

        view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

        layer = pdk.Layer(
            type="PathLayer",
            data=df,
            pickable=True,
            get_color="color",
            width_scale=1,
            width_min_pixels=1,
            get_path="path",
            get_width=1,
        )
        st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))

    with b:
        df2 = df3[df3['ac_type'].isin(["IN_PASSENGER_VEHICLE"])]

        df2 = df2.drop_duplicates(subset=['time_stamp','lat','lon'], keep='last')
        df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
        df2['day'] = df2['time_stamp'].map(get_day)
        df2['month'] = df2['time_stamp'].map(get_month)
        df2['hour'] = df2['time_stamp'].map(get_hour)
        df2.drop_duplicates(subset=['month', 'day', 'hour'])

        df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
        df2['date'] = df2['time_stamp'].map(convertToDateString)
        df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

        df = pd.DataFrame({
            "name": df2["date"].values.tolist(),
            "color": [(250, 166,26) for i in range(len(df2))],
            "path": df2['path'].values.tolist()
        })

        st.subheader('IN_PASSENGER_VEHICLE 🚗')

        view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

        layer = pdk.Layer(
            type="PathLayer",
            data=df,
            pickable=True,
            get_color="color",
            width_scale=1,
            width_min_pixels=1,
            get_path="path",
            get_width=1,
        )
        st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))
    with c:
        df2 = df3[df3['ac_type'].isin(["IN_TRAIN"])]

        df2 = df2.drop_duplicates(subset=['time_stamp', 'lat', 'lon'], keep='last')
        df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
        df2['day'] = df2['time_stamp'].map(get_day)
        df2['month'] = df2['time_stamp'].map(get_month)
        df2['hour'] = df2['time_stamp'].map(get_hour)
        df2.drop_duplicates(subset=['month', 'day', 'hour'])

        df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
        df2['date'] = df2['time_stamp'].map(convertToDateString)
        df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

        df = pd.DataFrame({
            "name": df2["date"].values.tolist(),
            "color": [(14, 212 + i, 252) for i in range(len(df2))],
            "path": df2['path'].values.tolist()
        })

        st.subheader('IN_TRAIN 🚅')

        view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

        layer = pdk.Layer(
            type="PathLayer",
            data=df,
            pickable=True,
            get_color="color",
            width_scale=1,
            width_min_pixels=1,
            get_path="path",
            get_width=1,
        )
        st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))

    d, e, f = st.columns(3)
    with d:
        df2 = df3[df3['ac_type'].isin(["IN_SUBWAY"])]

        df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
        df2['day'] = df2['time_stamp'].map(get_day)
        df2['month'] = df2['time_stamp'].map(get_month)

        df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
        df2['date'] = df2['time_stamp'].map(convertToDateString)
        df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

        df = pd.DataFrame({
            "name": df2["date"].values.tolist(),
            "color": [(103, 133, 244) for i in range(len(df2))],
            "path": df2['path'].values.tolist()
        })

        st.subheader('IN_SUBWAY 🚉')

        view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

        layer = pdk.Layer(
            type="PathLayer",
            data=df,
            pickable=True,
            get_color="color",
            width_scale=1,
            width_min_pixels=1,
            get_path="path",
            get_width=1,
        )
        st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))

    with e:
        df2 = df3[df3['ac_type'].isin(["IN_BUS"])]

        df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
        df2['day'] = df2['time_stamp'].map(get_day)
        df2['month'] = df2['time_stamp'].map(get_month)

        df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
        df2['date'] = df2['time_stamp'].map(convertToDateString)
        df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

        df = pd.DataFrame({
            "name": df2["date"].values.tolist(),
            "color": [(255, 200, 134) for i in range(len(df2))],
            "path": df2['path'].values.tolist()
        })

        st.subheader('IN_BUS 🚌')

        view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

        layer = pdk.Layer(
            type="PathLayer",
            data=df,
            pickable=True,
            get_color="color",
            width_scale=1,
            width_min_pixels=1,
            get_path="path",
            get_width=1,
        )
        st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))
    with f:
        df2 = df3[df3['ac_type'].isin(["UNKNOWN_ACTIVITY_TYPE"])]

        df2['time_stamp'] = df2['time_stamp'].map(pd.to_datetime)
        df2['day'] = df2['time_stamp'].map(get_day)
        df2['month'] = df2['time_stamp'].map(get_month)

        df2 = df2.groupby(['day', 'month', 'ac_type'], as_index=False).agg(lambda x: list(x))
        df2['date'] = df2['time_stamp'].map(convertToDateString)
        df2['path'] = [columnStack(*a) for a in tuple(zip(df2["lat"], df2["lon"]))]

        df = pd.DataFrame({
            "name": df2["date"].values.tolist(),
            "color": [(157, 81, 255) for i in range(len(df2))],
            "path": df2['path'].values.tolist()
        })

        st.subheader('UNKNOWN_ACTIVITY_TYPE 🛸')

        view_state = pdk.ViewState(latitude=48.8388504, longitude=2.3863984, zoom=4)

        layer = pdk.Layer(
            type="PathLayer",
            data=df,
            pickable=True,
            get_color="color",
            width_scale=1,
            width_min_pixels=1,
            get_path="path",
            get_width=1,
        )
        st.pydeck_chart(pdk.Deck(layers=[layer], initial_view_state=view_state, tooltip={"text": "{name}"}))
