import numpy as np
import pandas as pd
import pydeck as pdk
import os
from datetime import datetime as dt
import matplotlib.pyplot as plt
import altair as alt
import seaborn as sns
import webbrowser
import streamlit as st
import time

import home
import state



def app():
    
    #open dataframe
    df_gps = pd.read_csv('data_lite.csv')

    #preparation of the dataframe
    df_gps['datetime'] = pd.to_datetime(df_gps['datetime'], errors='coerce')
    #df_gps = df_gps.drop_duplicates(subset=['lat', 'lon'], keep='last')
    df_gps = df_gps.reset_index(drop=True)


    # Set parameter map
    timelaps =  pdk.Layer(
                'HexagonLayer',
                data=df_gps,
                get_position='[lon, lat]',
                radius=1500,
                elevation_scale=500,
                elevation_range=[5, 500],
                pickable=True,
                extruded=True,
            )

    deck = pdk.Deck(
        layers=[timelaps],
        initial_view_state=pdk.ViewState(
            latitude=46.85,
            longitude=4.77,
            zoom=6,
            pitch=50,
        )
    )

    st.balloons()
    map = st.pydeck_chart(deck)
    for step in range(len(df_gps)):
        timelaps.data = df_gps.loc[df_gps.index < step**2]
        deck.update()
        map.pydeck_chart(deck)




    
